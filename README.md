# Development Tools

## Table of Contents

- [Setup](#setup)
  - [Mac Options](#mac-options)
  - [Chrome](#chrome)
- [Tools](#tools)
  - [Chrome Extensions](#chrome-extensions)
  - [Terminal](#terminal)
  - [Project Software](#project-software)
- [Programming Languages](#programming-languages)
  - [Node](#node)

## Setup

### Mac Options
- Disable Screenshot Drop Shadows `defaults write com.apple.screencapture disable-shadow -bool TRUE`
- Show Hidden Files and Folders `defaults write com.apple.finder AppleShowAllFiles -bool TRUE`
- Show Login Message `sudo defaults write /Library/Preferences/com.apple.loginwindow LoginwindowText "Welcome back, Philip"`

### Chrome
1. Download Chrome `curl -o ~/Downloads/googlechrome.dmg https://dl.google.com/chrome/mac/stable/GGRO/googlechrome.dmg`
2. Disable Google Chrome’s Two-Finger Swipe Navigation `defaults write com.google.Chrome.plist AppleEnableSwipeNavigateWithScrolls -bool FALSE`

## Tools

### Chrome Extensions

- [Octotree](https://github.com/buunguyen/octotree) - Code tree for GitHub.
- [OctoLinker](https://github.com/OctoLinker/OctoLinker) - Navigate through code on GitHub more efficiently.
![OctoLinker](https://user-images.githubusercontent.com/1393946/34275053-dca87148-e69b-11e7-9d8f-e43ce361755b.gif)
- [Pretty Pull Requests](https://github.com/Yatser/prettypullrequests) - A chrome extension to make reviewing pull requests in Github a little easier.
- [Refined GitHub](https://github.com/sindresorhus/refined-github) - Browser extension that simplifies the GitHub interface and adds useful features
- [GitHub Repo Size](https://github.com/harshjv/github-repo-size) - Chrome extension to display repository size on GitHub

### Terminal

- [iTerm2](https://www.iterm2.com/downloads.html)
  - [iTerm2-Color-Schemes](https://github.com/mbadolato/iTerm2-Color-Schemes)
    - Vaughn Color Scheme `curl -o ~/Downloads/Vaughn.itermcolors https://raw.githubusercontent.com/mbadolato/iTerm2-Color-Schemes/master/schemes/Vaughn.itermcolors`
![Vaughn](https://github.com/mbadolato/iTerm2-Color-Schemes/raw/master/screenshots/vaughn.png)    

- [Zsh](https://github.com/robbyrussell/oh-my-zsh/wiki/Installing-ZSH)
  - [oh-my-zsh](https://github.com/robbyrussell/oh-my-zsh)
- [SpaceVim](https://github.com/SpaceVim/SpaceVim)
- [Travis CLI](https://github.com/travis-ci/travis.rb)
- `brew install protobuf`

### Project Software

- [Slack](https://slack.com/downloads)

## Programming Languages

### Node

1. Install [Node Version Manager](https://github.com/creationix/nvm)
2. ```nvm install node```

### Go
1. `brew install go && brew install dep`
